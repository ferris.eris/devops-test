# DevOps (a Kotlin & Ktor project)

1. delete Dockerfile if you have
2. create a gradlew running ``gradle wrapper`` (exclude ``gradlew``, ``gradle/wrapper/*`` from ``.gitignore``)
3. set the application port to 5000 (Heroku manner)
4. make a ``Procfile`` with ``web:`` entry (Heroku manner)